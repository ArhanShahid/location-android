package arhan.com.locationservice;

import android.Manifest;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class LocationService extends IntentService {

    //private static int time = 1000 * 10; // 10 sec
    //private static int time = 1000 * 20; // 20 sec
    //private static int time = 1000 * 30; // 30 sec
    private static int time = 1000 * 60; // 1 minute
    //private static int time = 1000 * 60 * 5; // 5 minute
    //private static int time = 1000 * 60 * 10; // 10 minute
    //private static int time = 1000 * 60 * 15; // 15 minute

    private LocationManager mLocationManager = null;
    private static final int LOCATION_INTERVAL = time;
    private static final float LOCATION_DISTANCE = 0;

    android.location.Location location;

    static final String TAG = "LocationService : ";

    public LocationService() {
        super("Location Service Worker Thread");
    }

    private void printLocationLog(String title,Location loc){
        String result = "Lat : " + String.valueOf(loc.getLatitude());
        result += "\nLng: " + String.valueOf(loc.getLongitude());
        Log.d(TAG, title);
        Log.d(TAG, result);
    };

    @Override
    public void onCreate() {
        super.onCreate();

        while (location == null) {
            location = getLocation();

        }
        printLocationLog("onCreate Location", location);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "onStartCommand");

        return START_STICKY;
    }

    private void requestData(RequestPackage p) {
        if (isOnline()) {
            new HttpTask().execute(p);
        } else {
            Log.e(TAG, "Oops! Network isn't available");
            Toast.makeText(getApplicationContext(),"Oops! Network isn't available", Toast.LENGTH_LONG).show();
            this.stopSelf();
        }

    }

    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }

    public class HttpTask extends AsyncTask<RequestPackage, String, String> {

        @Override
        protected String doInBackground(RequestPackage... params) {

            return HttpManager.getData(params[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.d(TAG, "Updated Successfully ! : " + result);
            Toast.makeText(getApplicationContext(),"Updated Successfully ! : "+result, Toast.LENGTH_LONG).show();

        }
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onDestroy() {
        if (mLocationManager != null) {
            for (int i = 0; i < mLocationListeners.length; i++) {
                try {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                        return;
                    }
                    mLocationManager.removeUpdates(mLocationListeners[i]);
                } catch (Exception ex) {
                    Log.i(TAG, "fail to remove location listners, ignore", ex);
                }
            }
        }
        Log.i(TAG,"onDestroy");
        super.onDestroy();
    }

    @Override
    protected void onHandleIntent(Intent intent) {

    }

    private class LocationListener implements android.location.LocationListener
    {
        Location mLastLocation;

        public LocationListener(String provider)
        {
            Log.i(TAG, "LocationListener " + provider);
            mLastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location)
        {
            mLastLocation.set(location);


            printLocationLog("onLocationChanged", location);

            String url = "https://trailsmeapp.herokuapp.com/api/insertLoc";

            RequestPackage p = new RequestPackage();
            JSONObject data = new JSONObject();
            try {
                data.put("lat", mLastLocation.getLatitude());
                data.put("lng",mLastLocation.getLongitude());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            p.setMethod("POST");
            p.setUri(url);
            p.setParam("data", data.toString());

            requestData(p);

        }

        @Override
        public void onProviderDisabled(String provider)
        {
            Log.i(TAG, "onProviderDisabled: " + provider);
        }

        @Override
        public void onProviderEnabled(String provider)
        {
            Log.i(TAG, "onProviderEnabled: " + provider);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras)
        {
            Log.i(TAG, "onStatusChanged: " + provider);
        }
    }

    LocationListener[] mLocationListeners = new LocationListener[] {
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };

    private void initializeLocationManager() {
        Log.i(TAG, "initializeLocationManager");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }

    private android.location.Location getLocation(){

        android.location.Location lastLocation = null;
        android.location.Location currentLocation = null;
        initializeLocationManager();
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[1]);

            if (mLocationManager != null) {

                lastLocation = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                if(lastLocation != null){
                    currentLocation = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                }


                //updateGPSCoordinates();
            }else {
                Log.i(TAG,"mLocationManager is null");
            }

        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "network provider does not exist, " + ex.getMessage());
        }
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[0]);
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "gps provider does not exist " + ex.getMessage());
        }

        return currentLocation;
    }


}
