package arhan.com.locationservice;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class HttpManager {

    static String TAG = "HttpManager";

    public static String getData(RequestPackage p){

        HttpURLConnection connection = null;
        BufferedReader reader = null;
        String uri = p.getUri();

        if(p.getMethod().equals("GET")){
            if(p.getEncodedParams() != null){
                uri += "?" + p.getEncodedParams();
            }
        }


        try{
            URL url = new URL(uri);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(p.getMethod());

            if(p.getMethod().equals("POST")){
                if(p.getEncodedParams() != null){
                    connection.setDoOutput(true);
                    OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                    writer.write(p.getEncodedParams());
                    writer.flush();
                }
            }


            StringBuffer buffer = new StringBuffer();
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String line;
            while ((line = reader.readLine()) != null){
                buffer.append(line);
            }

            return buffer.toString();
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }finally {
            if(connection != null) {
                connection.disconnect();
            }
            try {
                if(reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

    }

}
