package arhan.com.locationservice;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    static String TAG = "MainActivity";
    private final String serviceName = "arhan.com.locationservice.LocationService";
    Button startService;
    Button stopService;
    Button updateLocation;
    private TextView tvData;
    private TextView tvServiceStatus;

    private GPSTracker track = new GPSTracker(MainActivity.this);
    private Location myLocation = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startService = (Button) (findViewById(R.id.start_service));
        stopService = (Button) (findViewById(R.id.stop_service));
        updateLocation = (Button) (findViewById(R.id.update_location));
        tvData = (TextView) (findViewById(R.id.tvData));
        tvServiceStatus = (TextView) (findViewById(R.id.tvServiceStatus));


        tvServiceStatus.setText("Service is Running : "+isServiceRunning());

        updateLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.i(TAG, "Update Location");

                while (myLocation == null) {
                    myLocation = track.getLocation();
                }

                if (myLocation != null) {
                    String result = "Lat : " + String.valueOf(myLocation.getLatitude());
                    result += "\nLng: " + String.valueOf(myLocation.getLongitude());
                    tvData.setText(result);
                    //Toast.makeText(getApplicationContext(),result, Toast.LENGTH_LONG).show();
                    //String url = "http://192.168.10.6:3000/api/insertLoc";
                    String url = "https://trailsmeapp.herokuapp.com/api/insertLoc";

                    RequestPackage p = new RequestPackage();
                    JSONObject data = new JSONObject();
                    try {
                        data.put("lat", myLocation.getLatitude());
                        data.put("lng", myLocation.getLongitude());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    p.setMethod("POST");
                    p.setUri(url);
                    p.setParam("data", data.toString());

                    requestData(p);


                } else {
                    Toast.makeText(getApplicationContext(), "Oops! location not found", Toast.LENGTH_LONG).show();
                }


            }
        });



        startService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startService(new Intent(getApplicationContext(), LocationService.class));
                tvServiceStatus.setText("Service is Running : " + isServiceRunning());
                Toast.makeText(getApplicationContext(),"Service Started", Toast.LENGTH_LONG).show();


            }
        });

        stopService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isServiceRunning()){
                    stopService(new Intent(getApplicationContext(), LocationService.class));
                    tvServiceStatus.setText("Service is Running : "+isServiceRunning());
                    Toast.makeText(getApplicationContext(),"Service Stopped", Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(getApplicationContext(),"Service already Stopped", Toast.LENGTH_LONG).show();
                }



            }
        });


    }

    private boolean isServiceRunning(){
        boolean status = false;

        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for(ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)){
            if(serviceName.equals(service.service.getClassName())){
                return true;
            }

        }
        return status;
    }

    private void requestData(RequestPackage p) {
        if (isOnline()) {
            new HttpTask().execute(p);
        } else {
            Log.e(TAG, "Oops! Network isn't available");
            Toast.makeText(getApplicationContext(), "Oops! Network isn't available", Toast.LENGTH_LONG).show();
        }

    }

    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }
    public class HttpTask extends AsyncTask<RequestPackage,String,String> {

        @Override
        protected String doInBackground(RequestPackage... params) {

            return HttpManager.getData(params[0]);
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            Toast.makeText(getApplicationContext(),result+"Updated Successfully!", Toast.LENGTH_LONG).show();

            tvData.setText(result);
        }
    }
}
